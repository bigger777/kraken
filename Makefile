CC := gcc
FLAGS := -Wall

SRC := src/main.c \
       src/kraken.c \
       src/helpers.c \
       src/dmn.c \
       src/args.c \
       src/message_queue/message_queue.c

SRC_TEST_LUGIN := src/test_plugin.c \
                  src/helpers.c \
                  src/args.c \
                  src/message_queue/message_queue.c

SRC_AS_PLUGIN := src/workers/arp_spoofing/worker.c \
                 src/helpers.c \
                 src/args.c

SRC_SYN_PLUGIN := src/workers/syn_flood/worker.c \
                  src/helpers.c \
                  src/args.c \
                  src/message_queue/message_queue.c

INCLUDES := -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include

#export LD_LIBRARY_PATH=/usr/local/lib
LIBS := -lpcap -lpthread -lglib-2.0 -lmsgpackc -ldl

OUT := kraken

.PHONY: all

all: plugins
	$(CC) $(SRC) -o $(OUT) $(FLAGS) $(INCLUDES) $(LIBS) $(INCLUDES) -D DEBUG

plugins: arp_spoofing syn_flood

arp_spoofing:
	$(CC) -fPIC $(SRC_AS_PLUGIN) -shared -o ./plugins/libas_plugin.so $(LIBS) $(INCLUDES) -D DEBUG &&\
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD)/src/workers/arp_spoofing/kmod modules

syn_flood:
	$(CC) -fPIC $(SRC_SYN_PLUGIN) -shared -o ./plugins/libsyn_plugin.so $(LIBS) $(INCLUDES) -D DEBUG
