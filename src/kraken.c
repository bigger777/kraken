#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <dirent.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <signal.h>

#include <linux/if_ether.h>

#include "kraken.h"
#include "helpers.h"
#include "queues.h"

#define TIMEOUT 10000 /* milliseconds */

#define PROMISC_MODE_OFF 0
#define PROMISC_MODE_ON  1

#define PLUGINS_PATH "./plugins"

#define MASTER_PROCESS "master_process"
#define WORKER_MAIN    "worker_main"
#define WORKER_EXIT    "worker_exit"
#define WORKER_INIT    "worker_init"

#define PLUG_PATH_LEN 64

#define MASTER_OFFLINE "offline"

void * kraken_master(void * data)
{
	kraken_master_t * master = (kraken_master_t *) data;

	while(1)
	{
		struct pcap_pkthdr header;

		const u_char * packet = pcap_next(master->handle, &header);

		if (!packet) continue;

		FOREACH_KRAKEN_MASTERS_PROCESS(master, master_process, idx)
		{
			master_process(master, packet);
		}
	}

	pthread_exit(NULL);
}

static int kraken_start_workers(kraken_t * kraken)
{
	FOREACH_KRAKEN_WORKERS(kraken, worker, idx)
	{
		if (worker->worker_init)
		{
			worker->worker_init(worker);
		}

		if (pthread_create(&worker->tid, NULL, (void *) worker->worker_main, worker))
		{
			KRAKEN_ERR("pthread_create worker %" PRIu16 " failed", worker->id);
			return -1;
		}

		worker->id = idx;
	}

	return 0;
}

static int kraken_set_master_handle_online(kraken_master_t * master, const char * dev_name)
{
	char errbuf[PCAP_ERRBUF_SIZE];

	master->handle = pcap_open_live(dev_name, BUFSIZ, PROMISC_MODE_ON, TIMEOUT, errbuf);
	if (!master->handle)
	{
		KRAKEN_ERR("pcap_open_live '%s' failed: %s", master->dev_name, errbuf);
		return -1;
	}

	return 0;
}

static int kraken_start_masters(kraken_t * kraken)
{
	FOREACH_KRAKEN_MASTERS(kraken, master, idx)
	{
		if (pthread_create(&master->tid, NULL, kraken_master, master))
		{
			KRAKEN_ERR("pthread_create for '%s' failed", master->dev_name);
			pcap_close(master->handle);
			return -1;
		}
	}

	while (!*kraken->quit)
	{
		sleep(1);
	}

	return 0;
}

static uint16_t kraken_create_master(kraken_t * kraken, const char * dev_name)
{
	kraken_master_t * master = &kraken->masters[kraken->masters_count];

	strncpy(master->dev_name, dev_name, sizeof(master->dev_name));

	master->id = kraken->masters_count;

	return master->id;
}

static int kraken_create_masters_auto(kraken_t * kraken)
{
	char errbuf[PCAP_ERRBUF_SIZE];

	pcap_if_t * alldevs;

	if (PCAP_ERROR == pcap_findalldevs(&alldevs, errbuf))
	{
		KRAKEN_ERR("pcap_findalldevs failed: %s", errbuf);
		return -1;
	}

	for (pcap_if_t * dev = alldevs; NULL != dev; dev = dev->next)
	{
		if (!strcmp(dev->name, ANY_DEVICE)) continue;

		KRAKEN_DBG("Create master '%s'", dev->name);

		uint16_t master_id = kraken_create_master(kraken, dev->name);

		if (kraken_set_master_handle_online(&kraken->masters[master_id], dev->name))
		{
			KRAKEN_ERR("kraken_set_master_handle_online '%s' failed", dev->name);
			return -1;
		}

		kraken->masters_count++;

		if (MASTERS_MAX == kraken->masters_count)
		{
			KRAKEN_ERR("Master limit (%d) reached", MASTERS_MAX);
			break;
		}
	}

	pcap_freealldevs(alldevs);

	return 0;
}

static int kraken_create_masters_custom(kraken_t * kraken)
{
	const char * dev_name = kraken->interfaces[0];

	while (dev_name[0])
	{
		KRAKEN_DBG("Create master '%s'", dev_name);

		uint16_t master_id = kraken_create_master(kraken, dev_name);

		if (kraken_set_master_handle_online(&kraken->masters[master_id], dev_name))
		{
			KRAKEN_ERR("kraken_set_master_handle_online '%s' failed", dev_name);
			return -1;
		}

		dev_name = kraken->interfaces[++kraken->masters_count];
	}

	return 0;
}

static int kraken_create_master_offline(kraken_t * kraken)
{
	char errbuf[PCAP_ERRBUF_SIZE];

	kraken_master_t * master = kraken->masters;

	master->handle = pcap_open_offline(kraken->pcap_path, errbuf);
	if (!master->handle)
	{
		KRAKEN_ERR("pcap_open_offline failed: %s", errbuf);
		return -1;
	}

	kraken_create_master(kraken, MASTER_OFFLINE);

	kraken->masters_count++;

	KRAKEN_DBG("Create master '%s'", master->dev_name);

	return 0;
}

static int kraken_create_masters(kraken_t * kraken)
{
	switch (kraken->ifmode)
	{
		case KRAKEN_IFACES_AUTO:
		{
			if (kraken_create_masters_auto(kraken))
			{
				KRAKEN_ERR("kraken_create_masters_auto failed");
				return -1;
			}
			return 0;
		}
		case KRAKEN_IFACES_CUSTOM:
		{
			if (kraken_create_masters_custom(kraken))
			{
				KRAKEN_ERR("kraken_create_masters_auto failed");
				return -1;
			}
			return 0;
		}
		case KRAKEN_IFACES_OFFLINE:
		{
			if (kraken_create_master_offline(kraken))
			{
				KRAKEN_ERR("kraken_create_masters_offline failed");
				return -1;
			}
			return 0;
		}
		default:
		{
			KRAKEN_ERR("Unknown interfaces mode");
			return -1;
		}
	}

	return 0;
}

static void kraken_terminate_masters(kraken_t * kraken)
{
	FOREACH_KRAKEN_MASTERS(kraken, master, idx)
	{
		pthread_cancel(master->tid);

		KRAKEN_DBG("Terminate master '%s'", master->dev_name);
	}
}

static void kraken_terminate_workers(kraken_t * kraken)
{
	FOREACH_KRAKEN_WORKERS(kraken, worker, idx)
	{
		pthread_cancel(worker->tid);

		if (worker->worker_exit)
		{
			worker->worker_exit();
		}

		KRAKEN_DBG("Terminate worker '%s'", worker->name);
	}
}

void kraken_start(kraken_t * kraken)
{
	KRAKEN_INFO("Kraken start");

	if (kraken_create_masters(kraken))
	{
		KRAKEN_ERR("kraken_create_masters");
		return;
	}

	if (kraken_start_workers(kraken))
	{
		KRAKEN_ERR("kraken_start_workers failed");
		return;
	}

	if (kraken_start_masters(kraken))
	{
		KRAKEN_ERR("kraken_start_masters");
		return;
	}

	kraken_terminate_masters(kraken);

	kraken_terminate_workers(kraken);

	KRAKEN_INFO("Kraken end");
}

static void kraken_load_plugins(kraken_t * kraken)
{
	DIR * d = opendir(PLUGINS_PATH);
	if (!d)
	{
		KRAKEN_ERR("opendir failed: %s", strerror(errno));
		exit(EXIT_FAILURE);
	}

	struct dirent * dir;

	uint16_t m_idx = 0;

	while ((dir = readdir(d)))
	{
		if ('.' == dir->d_name[0]) continue;

		char plug_path[PLUG_PATH_LEN];

		int rc = snprintf(plug_path, sizeof(plug_path), PLUGINS_PATH "/%s", dir->d_name);
		if ((rc < 0) || (rc >= sizeof(plug_path)))
		{
			KRAKEN_ERR("snprintf failed");
			exit(EXIT_FAILURE);
		}

		KRAKEN_INFO("Processing plugin '%s'", dir->d_name);

		void * handle = dlopen(plug_path, RTLD_LAZY);
		if (!handle)
		{
			KRAKEN_ERR("dlopen '%s' failed: %s", dir->d_name, dlerror());
			continue;
		}

		kraken->master_processes[m_idx] = dlsym(handle, MASTER_PROCESS);
		if (kraken->master_processes[m_idx])
		{
			KRAKEN_DBG("Found '%s' in '%s' (%p)",
			           MASTER_PROCESS, dir->d_name, kraken->master_processes[m_idx]);
			m_idx++;
		}

		kraken_worker_t * worker = &kraken->workers[kraken->workers_count];

		worker->worker_main = dlsym(handle, WORKER_MAIN);
		if (worker->worker_main)
		{
			KRAKEN_DBG("Found '%s' in '%s' (%p)",
			           WORKER_MAIN, dir->d_name, worker->worker_main);
			kraken->workers_count++;
		}

		worker->worker_exit = dlsym(handle, WORKER_EXIT);
		if (worker->worker_exit)
		{
			KRAKEN_DBG("Found '%s' in '%s' (%p)",
			           WORKER_EXIT, dir->d_name, worker->worker_exit);
		}

		worker->worker_init = dlsym(handle, WORKER_INIT);
		if (worker->worker_init)
		{
			KRAKEN_DBG("Found '%s' in '%s' (%p)",
			           WORKER_INIT, dir->d_name, worker->worker_init);
		}
	}

	kraken->masters_cb_count = m_idx;

	closedir(d);
}

static void kraken_init_master(kraken_master_t * master)
{
	master->id = 0;

	master->tid = 0;

	master->dev_name[0] = '\0';
}

static void kraken_init_masters(kraken_t * kraken)
{
	for (int m_idx = 0; m_idx < MASTERS_MAX; m_idx++)
	{
		kraken_master_t * master = &kraken->masters[m_idx];

		master->process_ptr = kraken->master_processes;

		kraken_init_master(master);
	}
}

static void kraken_init_worker(kraken_worker_t * worker)
{
	worker->id = 0;

	worker->tid = 0;

	worker->name[0] = '\0';

	worker->worker_init = NULL;
	worker->worker_main = NULL;
	worker->worker_exit = NULL;
}

static void kraken_init_workers(kraken_t * kraken)
{
	for (int m_idx = 0; m_idx < MASTERS_MAX; m_idx++)
	{
		kraken_worker_t * worker = &kraken->workers[m_idx];

		kraken_init_worker(worker);
	}
}

void kraken_init(kraken_t * kraken)
{
	kraken->ifmode = KRAKEN_IFACES_AUTO;

	kraken->quit = NULL;

	kraken->pid = 0;

	kraken->daemonize = false;

	kraken->workers_count = 0;
	kraken->masters_count = 0;

	kraken->masters_cb_count = 0;

	for (int idx = 0; idx < PLUGIN_MASTERS_MAX; idx++)
	{
		kraken->master_processes[idx] = NULL;
	}

	for (int idx = 0; idx < MASTERS_MAX; idx++)
	{
		memset(kraken->interfaces[idx], 0, sizeof(kraken->interfaces[idx]));
	}

	memset(kraken->pcap_path, 0, sizeof(kraken->pcap_path));

	kraken_init_masters(kraken);

	kraken_init_workers(kraken);

	kraken_load_plugins(kraken);
}

void kraken_fini(kraken_t * kraken)
{
	if (kraken->daemonize)
	{
		closelog();
	}
}
