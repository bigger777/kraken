#ifndef _DMN_H_
#define _DMN_H_

#include "kraken.h"

void daemonize(kraken_t * kraken);

#endif // _DMN_H_
