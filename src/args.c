#include <getopt.h>
#include <sys/types.h>
#include <stdbool.h>
#include <stdlib.h>

#include "args.h"
#include "helpers.h"

#define IFACE_DELIM ','

bool syslog_enable = false;

static void parse_interfaces(kraken_t * kraken, char * ifaces)
{
	KRAKEN_DBG("Parse interfaces");

	char * if_ptr = ifaces;

	int if_idx = 0;

	while(if_ptr)
	{
		if (MASTERS_MAX == if_idx)
		{
			KRAKEN_ERR("Master limit (%d) reached", MASTERS_MAX);
			break;
		}

		if_ptr = strchr(ifaces, IFACE_DELIM);

		if (if_ptr)
		{
			strncpy(kraken->interfaces[if_idx], ifaces, if_ptr - ifaces);

			ifaces = if_ptr + 1;

		}
		else
		{
			strncpy(kraken->interfaces[if_idx], ifaces, strlen(ifaces));
		}

		KRAKEN_DBG("Set intefface '%s'", kraken->interfaces[if_idx]);

		if_idx++;
	}

	KRAKEN_DBG("Parse interfaces done");
}

static void print_help(void)
{
	KRAKEN_INFO("Usage: kraken [option]\n");
	KRAKEN_INFO("GENERAL OPTIONS");
	KRAKEN_INFO("    -d  --daemonize                 Daemonize process");
	KRAKEN_INFO("    -i  --interface <if1,if2,...>   Use the specified network interfaces");
	KRAKEN_INFO("    -p  --pcap                      Use pcap file for processing");
	KRAKEN_INFO("    -h  --help                      Print help information");
}

void parse_args(kraken_t * kraken, int argc, char ** argv)
{
	const struct option long_opt[] = {
		{"daemonize",  no_argument,        0,  'd'},
		{"interface",  required_argument,  0,  'i'},
		{"file",       required_argument,  0,  'f'},
		{"help",       no_argument,        0,  'h'},
		{NULL,         0,                  0,   0}
	};

	const char * short_opt = "di:p:h";

	int opt;

	while ((opt = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
	{
		switch (opt)
		{
			case 'h':
			{
				print_help();
				exit(EXIT_SUCCESS);
			}
			case 'i':
			{
				kraken->ifmode = KRAKEN_IFACES_CUSTOM;
				parse_interfaces(kraken, optarg);
				break;
			}
			case 'd':
			{
				kraken->daemonize = true;
				break;
			}
			case 'p':
			{
				kraken->ifmode = KRAKEN_IFACES_OFFLINE;
				strncpy(kraken->pcap_path, optarg, sizeof(kraken->pcap_path));
				break;
			}
			default:
			{
				KRAKEN_ERR("Invalid option argument %d", opt);

				print_help();

				exit(EXIT_SUCCESS);
			}
		}
	}
}
