#include <unistd.h>
#include <ctype.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <linux/if.h>
#include <linux/if_ether.h>
#include <linux/netdevice.h>

#include "helpers.h"

static void print_hex_ascii_line(const u_char * payload, const size_t len, const int offset)
{
	/* offset */
	printf("%05d   ", offset);

	/* hex */
	const u_char * ch = payload;

	for (int i = 0; i < len; i++)
	{
		printf("%02x ", *ch);
		ch++;

		/* print extra space after 8th byte for visual aid */
		if (i == 7)
		{
			printf(" ");
		}
	}

	/* print space to handle line less than 8 bytes */
	if (len < 8)
	{
		printf(" ");
	}

	/* fill hex gap with spaces if not full line */
	if (len < 16)
	{
		int gap = 16 - len;

		for (int i = 0; i < gap; i++)
		{
			printf("   ");
		}
	}

	printf("   ");

	/* ascii (if printable) */
	ch = payload;

	for (int i = 0; i < len; i++)
	{
		if (isprint(*ch))
		{
			printf("%c", *ch);
		}
		else
		{
			printf(".");
		}

		ch++;
	}

	printf("\n");
}

void print_payload(const u_char * payload, const size_t len)
{

	int len_rem = len;
	int line_width = 16; /* number of bytes per line */
	int line_len;
	int offset = 0; /* zero-based offset counter */
	const u_char * ch = payload;

	if (len <= 0)
	{
		return;
	}

	/* data fits on one line */
	if (len <= line_width)
	{
		print_hex_ascii_line(ch, len, offset);
		return;
	}

	/* data spans multiple lines */
	while(1)
	{
		/* compute current line length */
		line_len = line_width % len_rem;

		/* print line */
		print_hex_ascii_line(ch, line_len, offset);

		/* compute total remaining */
		len_rem = len_rem - line_len;

		/* shift pointer to remaining bytes to print */
		ch = ch + line_len;

		/* add offset */
		offset = offset + line_width;

		/* check if we have line width chars or less */
		if (len_rem <= line_width)
		{
			/* print last line and get out */
			print_hex_ascii_line(ch, len_rem, offset);
			break;
		}
	}
}

void print_ip(const uint32_t ip)
{
	uint8_t bytes[4];

	bytes[0] = ip & 0xff;
	bytes[1] = (ip >> 8) & 0xff;
	bytes[2] = (ip >> 16) & 0xff;
	bytes[3] = (ip >> 24) & 0xff;

	printf("%d.%d.%d.%d\n", bytes[3], bytes[2], bytes[1], bytes[0]);
}

int get_ip_mac_address(const char * dev_name, uint32_t * ip, uint8_t * mac)
{
	int rc = -1;

	int fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (-1 == fd)
	{
		KRAKEN_ERR("socket failed: %s", strerror(errno));
		return -1;
	}

	struct ifreq ifr;

	strncpy(ifr.ifr_name, dev_name, IFNAMSIZ);

	if (ioctl(fd, SIOCGIFADDR, &ifr))
	{
		KRAKEN_ERR("ioctl failed: %s", strerror(errno));
		goto exit;
	}

	*ip = ((struct sockaddr_in *) &ifr.ifr_addr)->sin_addr.s_addr;

	if (ioctl(fd, SIOCGIFHWADDR, &ifr))
	{
		KRAKEN_ERR("ioctl failed: %s", strerror(errno));
		goto exit;
	}

	for (int idx = 0; idx < ETH_ALEN; idx++)
	{
		mac[idx] = ifr.ifr_addr.sa_data[idx];
	}

	rc = 0;
exit:
	close(fd);

	return rc;
}

const char * stringize_ip(const uint32_t ip_net)
{
	uint32_t ip = ntohl(ip_net);

	static char ip_buf[INET_ADDRSTRLEN];

	int rc = snprintf(ip_buf, sizeof(ip_buf), "%hhu.%hhu.%hhu.%hhu",
	                  (ip >> 24) & 0xff,
	                  (ip >> 16) & 0xff,
	                  (ip >> 8) & 0xff,
	                  ip & 0xff);

	if ((-1 == rc) || (rc >= sizeof(ip_buf)))
	{
		KRAKEN_ERR("snprintf failed: %d", rc);
		return NULL;
	}

	return ip_buf;
}

const char * stringize_mac(const uint8_t * mac)
{
	static char mac_buf[MAX_ADDR_LEN];

	int rc = snprintf(mac_buf, sizeof(mac_buf), "%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx",
	         mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

	if ((-1 == rc) || (rc >= sizeof(mac_buf)))
	{
		KRAKEN_ERR("snprintf failed: %d", rc);
		return NULL;
	}

	return mac_buf;
}
