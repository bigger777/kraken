#include <stdio.h>

#include "kraken.h"
#include "helpers.h"
#include "dmn.h"
#include "args.h"

int main(int argc, char ** argv)
{
	KRAKEN_DBG("The Kraken is born");

	kraken_t kraken;

	kraken_init(&kraken);

	parse_args(&kraken, argc, argv);

	daemonize(&kraken);

	kraken_start(&kraken);

	kraken_fini(&kraken);

	KRAKEN_DBG("The Kraken exited");

	return 0;
}
