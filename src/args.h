#ifndef _ARGS_H_
#define _ARGS_H_

#include "kraken.h"

void parse_args(kraken_t * kraken, int argc, char ** argv);

#endif // _ARGS_H_
