#ifndef _HELPERS_H_
#define _HELPERS_H_

#include <stdio.h>
#include <syslog.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <pcap.h>
#include <stdint.h>

#define KRAKEN_NAME "Kraken"

extern bool syslog_enable;

#define KRAKEN_LOG(__stream__, __log__, __fmt__, ...) \
	do { \
		if (syslog_enable) \
		{ \
			syslog(__log__, __fmt__, ##__VA_ARGS__); \
		} \
		else \
		{ \
			fprintf(__stream__, __fmt__, ##__VA_ARGS__); \
		} \
	} while(0)

/* Debug */
#if defined DEBUG
// fprintf(stdout, "[DEBUG] <%s> " _fmt_ "\n", __func__, ##__VA_ARGS__);
#define KRAKEN_DBG(__fmt__, ...) \
	KRAKEN_LOG(stdout, LOG_DEBUG, "\033[0;32m[DEBUG]\033[0m <%s> " __fmt__ "\n", __func__, ##__VA_ARGS__)
#else
#define KRAKEN_DBG(...)
#endif

// fprintf(stderr, "[ERROR] <%s> " _fmt_ "\n", __func__, ##__VA_ARGS__);
#define KRAKEN_ERR(__fmt__, ...) \
	KRAKEN_LOG(stderr, LOG_ERR, "\033[1;31m[ERROR]\033[0m <%s> " __fmt__ "\n", __func__, ##__VA_ARGS__)

// fprintf(stdout, _fmt_ "\n", ##__VA_ARGS__);
#define KRAKEN_INFO(__fmt__, ...) \
	KRAKEN_LOG(stdout, LOG_INFO, "\033[0;34m[INFO]\033[0m "__fmt__ "\n", ##__VA_ARGS__)

#define IP_HL(__ip_header__) ((__ip_header__->ihl) * 4)

void print_payload(const u_char * payload, const size_t len);

void print_ip(const uint32_t ip);

int get_ip_mac_address(const char * dev_name, uint32_t * ip, uint8_t * mac);

const char * stringize_ip(const uint32_t ip);

const char * stringize_mac(const uint8_t * mac);

#endif // _HELPERS_H_
