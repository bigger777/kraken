#ifndef _KRAKEN_H_
#define _KRAKEN_H_

#include <stdbool.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pcap.h>
#include <linux/if.h>

#define ANY_DEVICE "any"

#define MASTERS_MAX 24
#define WORKERS_MAX 64

#define PLUGIN_MASTERS_MAX 64
#define PLUGIN_WORKERS_MAX 64

#define DEV_NAME_LEN 32

#define WORKER_NAME_LEN 32

#define PCAP_PATH_LEN 64

typedef void (*master_process_ptr)(const void *, const void *);

typedef void (*worker_main_ptr)(void);
typedef void (*worker_exit_ptr)(void);
typedef void (*worker_init_ptr)(void *);

typedef enum
{
	KRAKEN_IFACES_AUTO,
	KRAKEN_IFACES_CUSTOM,
	KRAKEN_IFACES_OFFLINE
} kraken_ifmode_t;

typedef struct kraken_master
{
	char dev_name[DEV_NAME_LEN];

	uint16_t id;

	pthread_t tid;

	pcap_t * handle;

	master_process_ptr * process_ptr;
} kraken_master_t;

typedef struct
{
	char name[WORKER_NAME_LEN];

	uint16_t id;

	pthread_t tid;

	worker_init_ptr worker_init;
	worker_main_ptr worker_main;
	worker_exit_ptr worker_exit;
} kraken_worker_t;

typedef struct
{
	kraken_ifmode_t ifmode;

	bool * quit;

	pid_t pid;

	bool daemonize;

	uint16_t masters_count;
	uint16_t workers_count;

	uint16_t masters_cb_count;
	uint16_t workers_cb_count;

	kraken_master_t masters[MASTERS_MAX];
	kraken_worker_t workers[WORKERS_MAX];

	char interfaces[MASTERS_MAX][DEV_NAME_LEN];

	char pcap_path[PCAP_PATH_LEN];

	master_process_ptr master_processes[PLUGIN_MASTERS_MAX];
} kraken_t;

void kraken_start(kraken_t * kraken);

void kraken_init(kraken_t * kraken);

void kraken_fini(kraken_t * kraken);

#define FOREACH_KRAKEN_MASTERS(__kraken__, __master__, __idx__) \
		int __idx__ = 0; \
		for (kraken_master_t * __master__ = __kraken__->masters; \
		     __idx__ < __kraken__->masters_count; \
		     __master__ = &__kraken__->masters[++__idx__]) \

#define FOREACH_KRAKEN_WORKERS(__kraken__, __worker__, __idx__) \
		int __idx__ = 0; \
		for (kraken_worker_t * __worker__ = __kraken__->workers; \
		     __idx__ < __kraken__->workers_count; \
		     __worker__ = &__kraken__->workers[++__idx__]) \

#define FOREACH_KRAKEN_MASTERS_PROCESS(__master__, __master_cb__, __idx__) \
	int __idx__ = 0; \
	for (master_process_ptr __master_cb__ = __master__->process_ptr[idx]; \
	     __master__->process_ptr[__idx__] && __idx__ < PLUGIN_MASTERS_MAX; \
	     __master_cb__ = __master__->process_ptr[++__idx__])

#define KRAKEN_SET_WORKER_NAME(__worker__, __name__) \
	strncpy(__worker__->name, __name__, sizeof(__worker__->name)); \
	KRAKEN_DBG("Start worker '%s'", __name__);

#endif // _KRAKEN_H_
