#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <pthread.h>

#include "worker.h"
#include "kmod/arp_spoofing_ctl.h"

#include "../../kraken.h"
#include "../../helpers.h"

#define ARP_SPOOFING_MODULE_NAME MODULE_NAME

#define ARP_SPOOFING_MODULE_PATH \
	"./src/workers/arp_spoofing/kmod/" ARP_SPOOFING_MODULE_NAME ".ko"

#define WORKER_NAME "ARP-spoofing worker"

static int get_ifnames(arp_spoofing_ifnames_t * ifnames)
{
	char errbuf[PCAP_ERRBUF_SIZE];

	pcap_if_t * alldevs;

	if (PCAP_ERROR == pcap_findalldevs(&alldevs, errbuf))
	{
		KRAKEN_ERR("pcap_findalldevs failed: %s", errbuf);
		return -1;
	}

	int idx = 0;

	for (pcap_if_t * dev = alldevs; NULL != dev; dev = dev->next)
	{
		if (!strcmp(dev->name, ANY_DEVICE)) continue;

		char * ifname = ifnames->ifname[idx];

		strncpy(ifname, dev->name, DEV_NAME_LEN);

		idx++;
	}

	pcap_freealldevs(alldevs);

	return 0;
}

static int arp_spoofing_ctl_send(arp_spoofing_ifnames_t * ifnames)
{
	int ret = -1;

	int fd = open(ARP_SPOOFING_AUTH_DEV_PATH, O_WRONLY);
	if (-1 == fd)
	{
		KRAKEN_ERR("open failed: %s", strerror(errno));
		return -1;
	}

	if (ioctl(fd, AS_IOCTL_IFACES, ifnames))
	{
		KRAKEN_ERR("ioctl failed: %s", strerror(errno));
		goto exit;
	}

	ret = 0;
exit:

	close(fd);

	return ret;
}

// TODO
int wait_alert(void)
{
	int ret = -1;

	int fd = open(ARP_SPOOFING_AUTH_DEV_PATH, O_RDONLY);
	if (-1 == fd)
	{
		KRAKEN_ERR("open failed: %s", strerror(errno));
		goto exit;
	}

	while (1)
	{
		/*if (ioctl(fd, AS_IOCT_ALERT, NULL))
		{
			KRAKEN_ERR("ioctl failed: %s", strerror(errno));
			goto exit;
		}

		KRAKEN_INFO("ARP spoofing detected");*/
		sleep(10);
	}

	ret = 0;

exit:

	close(fd);

	return ret;
}

static void insert_module()
{
	system("insmod " ARP_SPOOFING_MODULE_PATH);
}

void worker_exit(void)
{
	system("rmmod " ARP_SPOOFING_MODULE_NAME);
}

void * worker_main(void * data)
{
	kraken_worker_t * worker = (kraken_worker_t *) data;

	KRAKEN_SET_WORKER_NAME(worker, WORKER_NAME);

	insert_module();

	arp_spoofing_ifnames_t ifnames;

	if (get_ifnames(&ifnames))
	{
		KRAKEN_ERR("%s: get_ifnames failed", WORKER_NAME);
		goto exit;
	}

	if (arp_spoofing_ctl_send(&ifnames))
	{
		KRAKEN_ERR("%s: arp_spoofing_ctl_send failed", WORKER_NAME);
		goto exit;
	}

exit:
	pthread_exit(NULL);
}
