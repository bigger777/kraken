#ifndef ARP_SPOOFING_H
#define ARP_SPOOFING_H

#define LOG_FACILITY "ARP_SPOOFING: "

#include "arp_spoofing_ctl.h"

#define AS_DBG(__fmt__, ...) printk(KERN_DEBUG LOG_FACILITY  __fmt__ "\n", ##__VA_ARGS__)
#define AS_LOG(__fmt__, ...) printk(KERN_INFO LOG_FACILITY __fmt__ "\n", ##__VA_ARGS__)
#define AS_ERR(__fmt__, ...) printk(KERN_ERR LOG_FACILITY __fmt__ "\n", ##__VA_ARGS__)

extern spinlock_t if_lock;
extern int ifindexes[ARP_SPOOFING_IFACES_MAX];

#endif // ARP_SPOOFING_H
