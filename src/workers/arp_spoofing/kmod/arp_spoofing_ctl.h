#ifndef ARP_SPOOFING_CTL_H
#define ARP_SPOOFING_CTL_H

#include <linux/if.h>

#define MODULE_NAME "arp_spoofing"

#define ARP_SPOOFING_DEVICE_NAME "arp_spoofing"
#define ARP_SPOOFING_AUTH_DEV_PATH "/dev/" ARP_SPOOFING_DEVICE_NAME
#define ARP_SPOOFING_CLASS_NAME ARP_SPOOFING_DEVICE_NAME "_class"

#define ARP_SPOOFING_MAJOR_NUM 401

#define ARP_SPOOFING_IFACES_MAX 32

#define DEVICE_NAME_LEN 32

typedef struct {
	char ifname[ARP_SPOOFING_IFACES_MAX][DEVICE_NAME_LEN];
} arp_spoofing_ifnames_t;

#define AS_IOCTL_IFACES _IOR(ARP_SPOOFING_MAJOR_NUM, 0, arp_spoofing_ifnames_t)
#define AS_IOCT_ALERT   _IOW(ARP_SPOOFING_MAJOR_NUM, 1, NULL)

int arp_spoofing_dev_init(void);

void destroy_dev_class(void);

#endif // ARP_SPOOFING_CTL_H
