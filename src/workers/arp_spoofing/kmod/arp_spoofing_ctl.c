#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter.h>
#include <linux/inetdevice.h>
#include <linux/netfilter_arp.h>
#include <linux/netfilter_ipv4.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#include <linux/cdev.h>

#include <net/neighbour.h>
#include <net/arp.h>
#include <net/route.h>

#include "arp_spoofing.h"

static long device_ioctl(struct file * file, unsigned int cmd, unsigned long arg);
#ifdef CONFIG_COMPAT
static long device_compat_ioctl(struct file * file, unsigned int cmd, unsigned long arg);
#endif

static dev_t dev;
static struct cdev cdev;
static struct device * device;
static struct class * class;

static struct file_operations fops = {
	.owner          = THIS_MODULE,
	.unlocked_ioctl = device_ioctl,
#ifdef CONFIG_COMPAT
	.compat_ioctl   = device_compat_ioctl,
#endif
};

#ifdef CONFIG_COMPAT
static long device_compat_ioctl(struct file * file, unsigned int cmd, unsigned long arg)
{
	return device_ioctl(file, cmd, arg);
}
#endif

static int get_ifinedx_by_ifname(const char * ifname, int * ifindex)
{
	int ret = -1;

	struct net_device * ndev = first_net_device(&init_net);
	if (!ndev)
	{
		AS_ERR("first_net_device failed");
		return -1;
	}

	read_lock(&dev_base_lock);
	while (ndev)
	{
		if (!strcmp(ifname, ndev->name))
		{
			*ifindex = ndev->ifindex;
			ret = 0;
			break;
		}
		ndev = next_net_device(ndev);
	}
	read_unlock(&dev_base_lock);

	return ret;
}

static char * as_devnode(struct device * dev, umode_t * mode)
{
	if (!mode)
		return NULL;

	*mode = 0777;

	return NULL;
}

static long device_ioctl(struct file * file, unsigned int cmd, unsigned long arg)
{
	int ret = -1;

	int idxes[ARP_SPOOFING_IFACES_MAX] = { 0 };

	arp_spoofing_ifnames_t * ifnames = kmalloc(sizeof(arp_spoofing_ifnames_t), GFP_KERNEL);
	if (!ifnames)
	{
		AS_ERR("kmalloc failed");
		return -1;
	}

	switch(cmd)
	{
		case AS_IOCTL_IFACES:
		{
			int idx;

			if (copy_from_user(ifnames, (void __user *) arg, sizeof(arp_spoofing_ifnames_t)))
			{
				AS_ERR("copy_from_user failed");
				goto exit;
			}

			for (idx = 0; idx < ARP_SPOOFING_IFACES_MAX; idx++)
			{
				const char * ifname = ifnames->ifname[idx];

				if ('\0' == ifname[0])
					break;

				if (get_ifinedx_by_ifname(ifname, &idxes[idx]))
				{
					AS_ERR("get ifinedx for '%s' failed", ifname);
					continue;
				}

				AS_LOG("Add ifname '%s'", ifname);
			}

			break;
		}
		default:
		{
			AS_ERR("unknown ioctl command: %u", cmd);
			goto exit;
		}
	}

	ret = 0;

	spin_lock_irq(&if_lock);
	memcpy(ifindexes, idxes, sizeof(ifindexes));
	spin_unlock_irq(&if_lock);

exit:
	kfree(ifnames);

	return ret;
}

int arp_spoofing_dev_init(void)
{
	if (alloc_chrdev_region(&dev, 0, 1, ARP_SPOOFING_DEVICE_NAME))
	{
		AS_ERR("alloc_chrdev_region failed");
		return -1;
	}

	cdev_init(&cdev, &fops);

	cdev.owner = THIS_MODULE;
	cdev.ops = &fops;

	if (cdev_add(&cdev, dev, 1))
	{
		AS_ERR("cdev_add failed");
		goto chrdev_unreg;
	}

	class = class_create(THIS_MODULE, ARP_SPOOFING_CLASS_NAME);
	if (IS_ERR(class))
	{
		AS_ERR("class_create failed");
		goto class_destroy;
	}

	class->devnode = as_devnode;

	device = device_create(class, NULL, dev, NULL, ARP_SPOOFING_DEVICE_NAME);
	if (IS_ERR(device))
	{
		AS_ERR("device_create failed");
		goto err;
	}

	return 0;

err:
	device_unregister(device);

class_destroy:
	class_destroy(class);

chrdev_unreg:
	unregister_chrdev_region(dev, 1);

	return -1;
}

void destroy_dev_class(void)
{
	device_unregister(device);
	class_destroy(class);
	unregister_chrdev_region(dev, 1);
}
