#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter.h>
#include <linux/inetdevice.h>
#include <linux/netfilter_arp.h>
#include <linux/netfilter_ipv4.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>

#include <net/neighbour.h>
#include <net/arp.h>
#include <net/route.h>

#include "arp_spoofing.h"

DEFINE_SPINLOCK(if_lock);

int ifindexes[ARP_SPOOFING_IFACES_MAX] = { 0 };

static struct nf_hook_ops arphkrcv;
static struct nf_hook_ops arphksnd;

static LIST_HEAD(iplist);
static DEFINE_SPINLOCK(ip_lock);

typedef struct
{
	struct list_head list;
	u32 ip;
} iplist_t;

static bool is_zeroes(const u8 * ha)
{
	const u16 * ptr = (u16 *) ha;

	return ((0x00 == ptr[0]) && (0x00 == ptr[1]) && (0x00 == ptr[2]));
}

static bool iplist_check(const u32 ip)
{
	bool ret = false;

	struct list_head * p, * n;
	iplist_t * node;

	spin_lock_irq(&ip_lock);
	list_for_each_safe(p, n, &iplist)
	{
		node = list_entry(p, iplist_t, list);

		if (node->ip == ip)
		{
			ret = true;
			break;
		}
	}
	spin_unlock_irq(&ip_lock);

	return ret;
}

static bool arp_is_garp(const u32 sip, const u32 tip, const u32 addr_type)
{
	return ((sip == tip) && (RTN_UNICAST == addr_type));
}

static void iplist_add(const u32 ip)
{
	iplist_t * node = kmalloc(sizeof(iplist_t), GFP_KERNEL);
	if (!node)
	{
		AS_ERR("kmalloc failed");
		return;
	}

	node->ip = ip;

	spin_lock_irq(&ip_lock);
	list_add(&node->list, &iplist);
	spin_unlock_irq(&ip_lock);
}

static void iplist_del(const u32 ip)
{
	struct list_head * p, * n;
	iplist_t * node;

	spin_lock_irq(&ip_lock);
	list_for_each_safe(p, n, &iplist)
	{
		node = list_entry(p, iplist_t, list);

		if (node->ip == ip)
		{
			list_del(&node->list);
			kfree(node);
		}
	}
	spin_unlock_irq(&ip_lock);
}

static void send_garp_req(
		struct net_device * dev,
		struct in_device  * in_dev,
		const u32           dest_ip)
{
	struct in_ifaddr * ifa = in_dev->ifa_list;

	if (ifa)
	{
		arp_send(ARPOP_REQUEST, ETH_P_ARP, dest_ip, dev, ifa->ifa_address, NULL,
		         dev->dev_addr, NULL);
	}
}

static bool arp_is_probe(const u8 * tha, const u32 sip)
{
	return (is_zeroes(tha) && (0 == sip));
}

static bool req_for_us(struct in_device * in_dev, const u32 tip)
{
	struct in_ifaddr * ifa;

	for (ifa = in_dev->ifa_list; ifa; ifa = ifa->ifa_next)
	{
		if (tip == ifa->ifa_address)
			return true;
	}

	return false;
}

static void arp_parse_hdr(
		const struct arphdr *  arp,
		u32                 *  sip,
		u32                 *  tip,
		u8                  ** sha,
		u8                  ** tha,
		u8                  *  arpop,
		const u8               addr_len)
{
	u8 * arp_ptr = (u8 *) arp;
	arp_ptr += sizeof(struct arphdr);

	*arpop = ntohs(arp->ar_op);

	*sha = arp_ptr;
	arp_ptr += addr_len;

	memcpy(sip, arp_ptr, sizeof(u32));
	arp_ptr += sizeof(u32);

	*tha = arp_ptr;
	arp_ptr += addr_len;

	memcpy(tip, arp_ptr, sizeof(u32));
}

static bool check_iface(const int if_idx)
{
	int idx;

	bool ret = false;

	spin_lock_irq(&if_lock);
	for (idx = 0; idx < ARP_SPOOFING_IFACES_MAX; idx++)
	{
		if (if_idx == ifindexes[idx])
		{
			ret = true;
			break;
		}

		if (0 == ifindexes[idx])
			break;
	}
	spin_unlock_irq(&if_lock);

	return ret;
}

unsigned int arphook_rcv(
		void                       * priv,
		struct sk_buff             * skb,
		const struct nf_hook_state * state)
{
	struct arphdr * arp = arp_hdr(skb);
	struct net_device * dev = skb->dev;
	struct net * dnet = dev_net(dev);
	struct in_device * indev = in_dev_get(dev);
	struct neighbour * n;

	u8 * sha, * tha;
	u32 sip, tip, addr_type;
	u8 arpop;

	u8 status = NF_DROP;

	AS_LOG("Get ARP!");
	if (!check_iface(dev->ifindex))
	{
		return NF_ACCEPT;
	}

	arp_parse_hdr(arp, &sip, &tip, &sha, &tha, &arpop, dev->addr_len);

	addr_type = inet_addr_type_dev_table(dnet, dev, sip);
	n = neigh_lookup(&arp_tbl, &sip, dev);

	if (arp_is_garp(sip, tip, addr_type))
	{
		if (n)
		{
			send_garp_req(dev, indev, sip);
		}

		return NF_DROP;
	}

	/* Received ARP request */
	if (ARPOP_REQUEST == arpop)
	{
		/* Accept ARP probe requests */
		if (arp_is_probe(tha, sip))
		{
			status = NF_ACCEPT;
		}
		else if (n)
		{
			/* Neighbor exists and has same MAC - accept packet */
			if (!memcmp(n->ha, sha, dev->addr_len))
			{
				status = NF_ACCEPT;
			}
			/*
			 * Neighbor exists and has different MAC - send
			 * reply and request for authenticate host
			 */
			else if (req_for_us(indev, tip))
			{
				AS_LOG("Send for request for auth host");

				arp_send(ARPOP_REPLY, ETH_P_ARP, sip, dev, tip,
				         sha, dev->dev_addr, sha);

				arp_send(ARPOP_REQUEST, ETH_P_ARP, sip, dev, tip,
				         NULL, dev->dev_addr, NULL);
			}
		}
		/* Neighbor doesn't exist - accept packet */
		else
		{
			status = NF_ACCEPT;
		}
	}
	/* Received ARP REPLY */
	else if (ARPOP_REPLY == arpop)
	{
		/* 
		 * Accept packet if it exists in ip-list and delete such
		 * entry from the list
		 */
		if (iplist_check(sip))
		{
			iplist_del(sip);
			status = NF_ACCEPT;
		}
	}

	return status;
}

unsigned int arphook_snd(
		void                       * priv,
		struct sk_buff             * skb,
		const struct nf_hook_state * state)
{
	struct arphdr * arp = arp_hdr(skb);
	struct net_device * dev = skb->dev;
	struct net * dnet = dev_net(dev);

	u8 * sha, * tha;
	u32 sip, tip, addr_type;
	u8 arpop;

	if (!check_iface(dev->ifindex))
	{
		return NF_ACCEPT;
	}

	arp_parse_hdr(arp, &sip, &tip, &sha, &tha, &arpop, dev->addr_len);

	addr_type = inet_addr_type_dev_table(dnet, dev, sip);

	/* If sending ARP request - add target ip address to ip-list */
	if ((arpop == ARPOP_REQUEST) && !arp_is_garp(sip, tip, addr_type) &&
	    !iplist_check(tip))
	{
		iplist_add(tip);
	}

	return NF_ACCEPT;
}

static int __init arp_spoofing_init(void)
{
	AS_LOG("loaded");

	if (arp_spoofing_dev_init())
	{
		AS_ERR("arp_spoofing_dev_init failed");
		return -1;
	}

	/* Init ARP receive hook */
	arphkrcv.hook = arphook_rcv;
	arphkrcv.hooknum = NF_ARP_IN;
	arphkrcv.pf = NFPROTO_ARP;
	arphkrcv.priority = NF_IP_PRI_FIRST;
	nf_register_net_hook(&init_net, &arphkrcv);

	/* Init ARP send hook */
	arphksnd.hook = arphook_snd;
	arphksnd.hooknum = NF_ARP_OUT;
	arphksnd.pf = NFPROTO_ARP;
	arphksnd.priority = NF_IP_PRI_FIRST;
	nf_register_net_hook(&init_net, &arphksnd);

	return 0;
}

static void __exit arp_spoofing_exit(void)
{
	nf_unregister_net_hook(&init_net, &arphkrcv);
	nf_unregister_net_hook(&init_net, &arphksnd);

	destroy_dev_class();

	AS_LOG("unloaded");
}

module_init(arp_spoofing_init)
module_exit(arp_spoofing_exit)

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("ARP spoofing protection module");
MODULE_AUTHOR("@bigger777");
