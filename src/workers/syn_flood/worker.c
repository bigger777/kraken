#include <linux/tcp.h>
#include <linux/if_ether.h>
#include <linux/ip.h>
#include <pthread.h>
#include <time.h>

#include "../../helpers.h"
#include "worker.h"

#include "../../message_queue/message_queue.h"

#define QUEUE_MAX_DEPTH 128

#define CHECK_SESSIONS_INTERVAL 5 /* seconds*/

#define MAX_UNCONFIRMED_SESSIONS_COUNT 10000

#define UNCONFIRMED_SESSIONS_LIVE_INTERVAL 10 /* seconds */

#define WORKER_NAME "SYN-flood worker"

static uint32_t unconf_ses_count = 0;

static gint session_cmp_func(const list_entry_t * l, const list_entry_t * r)
{
	return !((l->saddr == r->saddr) && (l->daddr == r->daddr) &&
	         (l->sport == r->sport) && (l->dport == r->dport) &&
	         (l->dev_id == r->dev_id));
}

static void session_check_func(const list_entry_t * session, const time_t * cur_time)
{
	if ((*cur_time - session->time) >= UNCONFIRMED_SESSIONS_LIVE_INTERVAL)
	{
		unconf_ses_count++;
	}
}

static GList * list_remove_session_revert(GList * list, list_entry_t * session)
{
	for (GList * iter = list; iter; iter = g_list_next(iter))
	{
		list_entry_t * entry = iter->data;

		if ((entry->saddr == session->daddr) && (entry->daddr == session->saddr) &&
		    (entry->sport == session->dport) && (entry->dport == session->sport) &&
		    (entry->dev_id == session->dev_id))
		{
			return g_list_delete_link(list, iter);
		}
	}

	return list;
}

static int send_to_worker(
		const kraken_master_t * master,
		const struct iphdr    * ip,
		const struct tcphdr   * tcp,
		struct message_queue  * queue)
{
	q_message_t * message = message_queue_message_alloc_blocking(queue);
	if (!message)
	{
		KRAKEN_ERR("%s: message_queue_message_alloc_blocking failed", WORKER_NAME);
		return -1;
	}

	memcpy(message->dev_name, master->dev_name, sizeof(message->dev_name));

	message->dev_id = master->id;

	memcpy(&message->tcp, tcp, sizeof(struct tcphdr));
	memcpy(&message->ip, ip, sizeof(struct iphdr));

	message_queue_write(queue, message);

	return 0;
}

void master_process(const void * master_data, const void * packet_data)
{
	const u_char * packet = (u_char *) packet_data;
	const kraken_master_t * master = (kraken_master_t *) master_data;

	struct ethhdr * eth = (struct ethhdr *) packet;

	uint16_t proto = ntohs(eth->h_proto);
	if (ETH_P_IP != proto)
	{
		return;
	}

	struct iphdr * ip = (struct iphdr *) (packet + ETH_HLEN);

	if (IPPROTO_TCP != ip->protocol)
	{
		return;
	}

	struct tcphdr * tcp = (struct tcphdr *) (packet + ETH_HLEN + IP_HL(ip));

	if (send_to_worker(master, ip, tcp, &syn_queue))
	{
		KRAKEN_ERR("%s: send_to_worker failed", WORKER_NAME);
	}
}

void worker_init(void * data)
{
	kraken_worker_t * worker = (kraken_worker_t *) data;

	KRAKEN_SET_WORKER_NAME(worker, WORKER_NAME);

	if (message_queue_init(&syn_queue, sizeof(q_message_t), QUEUE_MAX_DEPTH))
	{
		KRAKEN_DBG("%s: message_queue_init failed", WORKER_NAME);
		pthread_exit(NULL);
	}
}

void * worker_main(void * data)
{
	time_t start_time = time(NULL);
	if (-1 == start_time)
	{
		KRAKEN_ERR("%s (%d): time failed", WORKER_NAME, __LINE__);
		pthread_exit(NULL);
	}

	GList * sessions_list = NULL;

	while(1)
	{
		q_message_t * message = message_queue_read(&syn_queue);

		time_t cur_time = time(NULL);
		if (-1 == cur_time)
		{
			KRAKEN_ERR("%s (%d): time failed", WORKER_NAME, __LINE__);
			break;
		}

		struct tcphdr * tcp = &message->tcp;
		struct iphdr * ip = &message->ip;

		const char * dev_name = message->dev_name;

		uint8_t dev_id = message->dev_id;

		list_entry_t session = {
			.saddr = ip->saddr,
			.daddr = ip->daddr,
			.sport = tcp->source,
			.dport = tcp->dest,
			.dev_id = dev_id,
			.time = cur_time
		};

		if (tcp->syn && !tcp->ack)
		{
			GList * session_ptr = g_list_find_custom(sessions_list,
			                                         &session,
			                                         (GCompareFunc) session_cmp_func);
			if (!session_ptr)
			{
				// TODO dynamic list --> static list
				list_entry_t * session_new = g_new(list_entry_t, 1);

				memcpy(session_new, &session, sizeof(list_entry_t));

				sessions_list = g_list_append(sessions_list, session_new);
			}
		}
		else if (tcp->ack && !tcp->syn)
		{
			sessions_list = list_remove_session_revert(sessions_list, &session);
		}
		else
		{
			message_queue_message_free(&syn_queue, message);
			continue;
		}

		if ((cur_time - start_time) > CHECK_SESSIONS_INTERVAL)
		{
			g_list_foreach(sessions_list, (GFunc) session_check_func, &cur_time);

			start_time = cur_time;

			KRAKEN_DBG("%s (%s): unconfirmed sessions %" PRIu32 "", WORKER_NAME, dev_name,
			           unconf_ses_count);
		}

		if (unconf_ses_count > MAX_UNCONFIRMED_SESSIONS_COUNT)
		{
			KRAKEN_INFO("%s: SYN flood attack detected on '%s'!", WORKER_NAME, dev_name);

			unconf_ses_count = 0;

			g_list_free(sessions_list);

			sessions_list = NULL;
		}

		message_queue_message_free(&syn_queue, message);
	}

	pthread_exit(NULL);
}
