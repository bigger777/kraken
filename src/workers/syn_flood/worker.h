#ifndef _WORKER_H_
#define _WORKER_H_

#include <glib.h>

#include "../../kraken.h"

struct message_queue syn_queue;

typedef struct
{
	char  dev_name[DEV_NAME_LEN];
	uint8_t dev_id;
	struct tcphdr tcp;
	struct iphdr ip;
} q_message_t;

typedef struct
{
	uint32_t saddr;
	uint32_t daddr;
	uint16_t sport;
	uint16_t dport;
	uint8_t dev_id;
	time_t time;
} list_entry_t;

#endif // _WORKER_H_
