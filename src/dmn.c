#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>

#include "dmn.h"
#include "helpers.h"

#define ROOT_DIR "/"
#define NULLDEV_PATH "/dev/null"

bool quit = false;

static void fork_twice(void)
{
	pid_t pid = fork();

	if (pid < 0)
	{
		KRAKEN_ERR("first fork failed");
		exit(EXIT_FAILURE);
	}

	/* Success: Let the parent terminate */
	if (pid > 0)
	{
		KRAKEN_DBG("Grandparent PID: %d exited", getpid());
		exit(EXIT_SUCCESS);
	}

	/* On success: The child process becomes session leader */
	if (setsid() < 0)
	{
		KRAKEN_ERR("setsid failed: %s", strerror(errno));
		exit(EXIT_FAILURE);
	}

	if ((signal(SIGCHLD, SIG_IGN) == SIG_ERR) || (signal(SIGHUP, SIG_IGN) == SIG_ERR))
	{
		KRAKEN_ERR("signal failed: %s", strerror(errno));
		exit(EXIT_FAILURE);
	}

	pid = fork();

	if (pid < 0)
	{
		KRAKEN_ERR("second fork failed");
		exit(EXIT_FAILURE);
	}

	/* Success: Let the parent terminate */
	if (pid > 0)
	{
		KRAKEN_DBG("Parent PID: %d exited", getpid());
		exit(EXIT_SUCCESS);
	}

	KRAKEN_DBG("Grandchild pid: %d", getpid());
}

static void set_dir(void)
{
	if (chdir(ROOT_DIR) == -1)
	{
		KRAKEN_ERR("chdir failed: %s", strerror(errno));
		exit(EXIT_FAILURE);
	}

	umask(0);
}

inline static void redirect_streams(void)
{
	int fd = open(NULLDEV_PATH, O_RDWR, 0);

	if (-1 == fd)
	{
		KRAKEN_ERR("open failed: %s", strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* Redirect the standard input to /dev/null */
	if (dup2(fd, STDIN_FILENO) == -1)
	{
		KRAKEN_ERR("dup2 failed: %s", strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* Redirect the standard output to /dev/null */
	if (dup2(fd, STDOUT_FILENO) == -1)
	{
		KRAKEN_ERR("dup2 failed: %s", strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* Redirect the standard error to /dev/null */
	if (dup2(fd, STDERR_FILENO) == -1)
	{
		KRAKEN_ERR("dup2 failed: %s", strerror(errno));
		exit(EXIT_FAILURE);
	}

	close(fd);
}

static void logging_init(void)
{
	syslog_enable = true;

	openlog(KRAKEN_NAME, LOG_PID, LOG_DAEMON);
}

static void handle_signal(int sig)
{
	quit = true;
}

static int signal_init(void)
{
	struct sigaction sa = { 0 };

	sa.sa_handler = handle_signal;
	sa.sa_flags = 0;

	sigaction(SIGTERM, &sa, NULL);
	sigaction(SIGINT, &sa, NULL);
	sigaction(SIGHUP, &sa, NULL);

	return 0;
}

void daemonize(kraken_t * kraken)
{
	if (kraken->daemonize)
	{
		KRAKEN_DBG("Daemonize Kraken");

		fork_twice();

		set_dir();

		redirect_streams();

		logging_init();
	}

	kraken->pid = getpid();

	kraken->quit = &quit;

	signal_init();
}
