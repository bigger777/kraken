#ifndef _QUEUES_H_
#define _QUEUES_H_

#include "message_queue/message_queue.h"

#define QUEUE_MAX_DEPTH 128

struct message_queue queue_tcp;

struct message_queue queue_ip;

struct message_queue queue_arp_spoofing;

#define DEV_NAME_LEN2 32
#define DATA_SIZE    128

typedef struct
{
	char    dev_name[DEV_NAME_LEN2];
	uint8_t data[DATA_SIZE];
} q_message_t;

#endif // _QUEUES_H_
